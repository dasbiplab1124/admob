import 'dart:io';

class AdHelper {
  static String get bannerAdUnitId {
    if (Platform.isAndroid) {
      return "ca-app-pub-1889440647912721/9104807208";
    } else if (Platform.isIOS) {
      return "ca-app-pub-1889440647912721/9104807208";
    } else {
      throw UnsupportedError('Unsupported platform');
    }
  }
}
